Teclado português brasileiro ABNT + ISO 9995-3
==============================================

O layout é mantido baseado em ABNT, mas com suporte adicional em ISO 9995-3
para tornar um layout de alfabeto latino internacional usando um teclado
padrão ABNT.

Com ele, podemos escrever em:

 1. Espanhol:
     - [~], [N] = ñ

 2. Francês: 
     - [`], [E] = è
     - [´], [E] = é
     - [^], [E] = ê
     - [¨], [E] = ë
     - AltGR + [A] = æ
     - AltGR + [O] = œ

 3. Alemão:
     - [¨], [U] = ü
     - AltGR + [S] = ß

 4. Esperanto:
     - [^], [C] = ĉ
     - AltGR + [`], [U] = ŭ

 5. Polonês:
     - [´], [S] = ś
     - AltGR + [-], [L] = ł
     - AltGR + Shift + [Ç], [A] = ą
     - AltGR + Shift + [E], [Z] = ż

 6. Norueguês/Dinamarquês:
     - AltGR + [A] = æ
     - AltGR + [-], [O] = ø
     - AltGR + [E], [A] = å

 7. Húngaro:
     - [¨], [O] = ö
     - AltGR + [´], [O] = ő

 8. Turco:
     - [^], [I] = î
     - [¨], [U] = ü
     - AltGR + [Ç], [S] = ş
     - AltGR + [`], [G] = ğ
     - AltGR + Shift + [E], Shift + [I] = İ
     - AltGR + Shift + [E], [I] = ı
